Repository created by REST api.

Method: curl -X POST -v -u user:pass https://api.bitbucket.org/2.0/repositories/username/repo_name_without_uppercase -d '{"scm":"git", "is_private":"true", "fork_policy":"no_public_forks"}'