#ifndef MASTER_H
#define MASTER_H

#include <QMainWindow>
#include <complex>
#include <QtConcurrent/QtConcurrent>

namespace Ui {
class master;
}

using namespace std;

typedef QList<QList<complex<double> > > GRID;
typedef QList<complex<double> > VECTOR;
typedef QList<QList<int> > CRACKS;

class master : public QMainWindow
{
    Q_OBJECT

public:
    explicit master(QWidget *parent = 0);
    ~master();

private:
    Ui::master *ui;

    unsigned int forDiEnd;
    unsigned int forDjEnd;

    int dimension = 0;
    int density = 0;
    int iterationCount = 0;
    int crackPointsNumber = 0;

    double rVal = 0.0;
    double cVal = 0.0;
    double pi = 0.0;
    double pulsation = 0.0;
    double frequency = 0.0;
    double tau = 0.0;
    double vcc = 0.0;

    complex<double> licznik;
    complex<double> mianownik;
    complex<double> mianownik2;
    complex<double> m, p;
    complex<double> timesTwo;
    complex<double> divFour;

    CRACKS cracksGrid;

    GRID mainGrid;
    GRID mainGridCopy;
    GRID densityGrid;
    GRID densityGridCopy;

    QFuture<int> future;
    QFutureWatcher<int> watcher;

    /* user defined functions */
    void clearGrids();
    void setDefaultValues();
    void primarySetup();
    void copyGrids(GRID &gridToCopy, GRID &gridCopy);
    void clearing(bool (&bools)[8], unsigned int &mode);
    void calculateLeftTop(int i, int j, int di, int dj);
    void calculateUpperRow(int di, int dj);
    void calculateRightTop(int i, int j, int di, int dj);
    void calculateLeftCol(int di, int dj);
    void calculateCenter(int di, int dj);
    void calculateRightCol(int di, int dj);
    void calculateLeftDown(int i, int j, int di, int dj);
    void calculateDownRow(int di, int dj);
    void calculateRightDown(int i, int j, int di, int dj);
    void densitySweep(int i, int j, int di, int dj);
    void nodeCalculus(int i, int j, int mode);
    void densityNodeCalculus(int i, int j, int di, int dj, int dMode);
    void generalFunction(int, int);
    void determineEnds(int i, int j, unsigned int &forDiEnd, unsigned int &forDjEnd);
    void calculateCurrents();
    void saveDataToFile(int saveMode);

    bool checkCrackPoint(int i, int j);
    bool checkRightToCrackPoint(int i, int j);
    bool checkBelowCrackPoint(int i, int j);
    bool checkBelowRightToCrackPoint(int i, int j);
    bool ifTop(int i);
    bool ifLeft(int j);
    bool ifRight(GRID &grid, int j);
    bool ifDensityRight(GRID &grid, int j);
    bool ifDown(GRID &grid, int i);

    int mainCalculationAlgorithm();
    int operationMode(bool &top, bool &left, bool &right, bool &down);
    int densityOperationMode(bool &top, bool &left, bool &right, bool &down);
    int nodeSurroundings(bool &top, bool &left, bool &right, bool &down);
    int densityNodeSurroundings(bool &top, bool &left, bool &right, bool &down);

    VECTOR curr1;
    VECTOR totalCurr;
    VECTOR wVect;
    VECTOR iVect;
    VECTOR freqVect;
    VECTOR pulsationVect;

    CRACKS drawCrackPoint();

    template<typename T>
    QList<QList<T> > createGrid(int &dimension);

    template<typename T>
    QList<QList<T> > createDensityGrid(int &density);

    template<typename Tt>
    void forCurrentWriteFile(QList<Tt> & forSave, QTextStream & out);

    template<typename T>
    void fillWithZeros(QList<QList<T> > &matrix);

private slots:
    void showOptionsScreen();
    void showHelpScreen();
    void showManualIterationStop();
    void setValuesForSimulation();
    void startCalculations();
    void exitProgram();
    void setSaveModeOne();
    void setSaveModeTwo();
    void setSaveModeThree();
    void handleFinished();

    void on_vccVal_valueChanged(double arg1);

public slots:
    void clearValues();
};

#endif // MASTER_H
