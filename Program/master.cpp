#include "master.h"
#include "ui_master.h"

#include <QMessageBox>
#include <ctime>
#include <complex>
#include <QtMath>
#include <QThread>
#include <QtConcurrent/QtConcurrent>
#include <QFileDialog>
#include <QLocale>

using namespace std;

master::master(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::master)
{
    ui->setupUi(this);

    primarySetup();

    connect(ui->exitProgram, SIGNAL(triggered(bool)), this, SLOT(exitProgram()));
    connect(ui->setOptions, SIGNAL(triggered(bool)), this, SLOT(showOptionsScreen()));
    connect(ui->showHelp, SIGNAL(triggered(bool)), this, SLOT(showHelpScreen()));
    connect(ui->manualStopLabel, SIGNAL(toggled(bool)), this, SLOT(showManualIterationStop()));
    connect(ui->setValuesBtn, SIGNAL(clicked(bool)), this, SLOT(setValuesForSimulation()));
    connect(ui->clearValuesBtn, SIGNAL(clicked(bool)), this, SLOT(clearValues()));
    connect(ui->clearValues, SIGNAL(triggered(bool)), this, SLOT(clearValues()));
    connect(ui->startCalcBtn, SIGNAL(clicked(bool)), this, SLOT(startCalculations()));
    connect(ui->saveCurrent, SIGNAL(triggered(bool)), this, SLOT(setSaveModeOne()));
    connect(ui->savePotentials, SIGNAL(triggered(bool)), this, SLOT(setSaveModeTwo()));
    connect(ui->saveAll, SIGNAL(triggered(bool)), this, SLOT(setSaveModeThree()));

    connect(&watcher, SIGNAL(finished()), this, SLOT(handleFinished()));

}

master::~master()
{
    delete ui;
}

void master::setDefaultValues()
{
    ui->capacitorVal->setValue(1);
    ui->resistorVal->setValue(1);
    ui->frequencyVal->setValue(10);
    ui->densityVal->setValue(3);
    ui->dimensionVal->setValue(3);
    ui->iterVal->setValue(50);
    ui->vccVal->setValue(1);
    totalCurr.push_back(0);
}

void master::clearGrids()
{
    mainGrid.clear();
    mainGridCopy.clear();
    densityGrid.clear();
    densityGridCopy.clear();
    cracksGrid.clear();
    curr1.clear();
    totalCurr.clear();
    totalCurr.push_back(0);

    iVect.clear();
    wVect.clear();
    freqVect.clear();
}

void master::primarySetup()
{
    ui->welcomeScreen->setVisible(true);
    ui->optionsScreen->setVisible(false);
    ui->helpScreen->setVisible(false);
    ui->iterLabel->setVisible(false);
    ui->iterVal->setVisible(false);
    ui->startCalcBtn->setVisible(false);
    ui->clearValuesBtn->setVisible(false);

    ui->saveAll->setEnabled(false);
    ui->saveCurrent->setEnabled(false);
    ui->savePotentials->setEnabled(false);

    setDefaultValues();

    srand ( unsigned ( time(0) ) );
}

void master::clearValues()
{
    setDefaultValues();
    clearGrids();

    QList<QDoubleSpinBox*> valBoxList = master::findChildren<QDoubleSpinBox *>();
    for(auto box : valBoxList)
    {
        box->setEnabled(true);
    }

    ui->clearValuesBtn->setVisible(false);
    ui->startCalcBtn->setVisible(false);

    ui->manualStopLabel->setEnabled(true);
    ui->startCalcBtn->setEnabled(true);
    ui->saveAll->setEnabled(false);
    ui->saveCurrent->setEnabled(false);
    ui->savePotentials->setEnabled(false);
}

void master::showManualIterationStop()
{
    if(! ui->iterLabel->isVisible())
    {
        ui->iterLabel->setVisible(true);
    }
    else ui->iterLabel->setVisible(false);

    if(! ui->iterVal->isVisible())
    {
        ui->iterVal->setVisible(true);
    }
    else ui->iterVal->setVisible(false);
}

void master::setValuesForSimulation()
{
    QList<QDoubleSpinBox*> valBoxList = master::findChildren<QDoubleSpinBox *>();
    for(auto box : valBoxList)
    {
        box->setEnabled(false);
    }
    ui->manualStopLabel->setEnabled(false);

    ui->clearValuesBtn->setVisible(true);
    ui->startCalcBtn->setVisible(true);

    dimension = ui->dimensionVal->value();
    density = ui->densityVal->value();
    rVal = ui->resistorVal->value();
    cVal = ui->capacitorVal->value();
    frequency = ui->frequencyVal->value();
    vcc = ui->vccVal->value();
    iterationCount = ui->iterVal->value();

    pi = M_PI;

    pulsation = 2*pi*frequency;
    tau = rVal*cVal;

    timesTwo.real(2);
    divFour.real(4);

    mainGrid = createGrid<complex<double> >(dimension);
    mainGridCopy = mainGrid;
    cracksGrid = createGrid<int>(dimension);
    densityGrid = createDensityGrid<complex<double> >(dimension);
    densityGridCopy = densityGrid;
}

void master::showOptionsScreen()
{
    ui->welcomeScreen->setVisible(false);
    ui->optionsScreen->setVisible(true);
    ui->helpScreen->setVisible(false);
}

void master::showHelpScreen()
{
    ui->welcomeScreen->setVisible(false);
    ui->optionsScreen->setVisible(false);
    ui->helpScreen->setVisible(true);
}

void master::startCalculations()
{
    ui->startCalcBtn->setEnabled(false);
    ui->setValuesBtn->setEnabled(false);
    ui->clearValues->setEnabled(false);
    ui->clearValuesBtn->setEnabled(false);

    ui->saveAll->setEnabled(false);
    ui->saveCurrent->setEnabled(false);
    ui->savePotentials->setEnabled(false);

    future = QtConcurrent::run(this, &master::mainCalculationAlgorithm);
    watcher.setFuture(future);

    //qDebug() << QString("%1").arg(future);
}

void master::handleFinished()
{
    QMessageBox::information(
                this,
                tr("Completed!"),
                tr("Calculations complete!") );

    ui->clearValuesBtn->setEnabled(true);
//    ui->clearValuesBtn->setVisible(true);
    ui->setValuesBtn->setEnabled(true);
    ui->clearValues->setEnabled(true);
    ui->saveAll->setEnabled(true);
    ui->saveCurrent->setEnabled(true);
    ui->savePotentials->setEnabled(true);
}

void master::exitProgram()
{
    QCoreApplication::quit();
}

template<typename T>
QList<QList<T> > master::createGrid(int &dimension)
{
    QList<T> listRow;
    QList<QList<T> > grid;

    for(int i=0; i<dimension; i++)
    {
        listRow.push_back(0);
    }

    for(int i=0; i<dimension+1; i++)
    {
        grid.push_back(listRow);
    }

    return grid;
}

template<typename T>
QList<QList<T> > master::createDensityGrid(int &dimension)
{
    QList<T> listRow;
    QList<QList<T> > grid;

    for(int i=0; i<dimension+1; i++)
    {
        listRow.push_back(0);
    }

    for(int i=0; i<dimension+1; i++)
    {
        grid.push_back(listRow);
    }

    return grid;
}

template<typename T>
void master::fillWithZeros(QList<QList<T> > &matrix)
{
    typename QList<QList<T> >::iterator row;
    typename QList<T>::iterator col;

    for(row=matrix.begin(); row!=matrix.end(); row++)
    {
        for(col=row->begin(); col!=row->end(); col++)
        {
            *col = 0;
        }
    }
}

void master::copyGrids(GRID &gridToCopy, GRID &gridCopy)
{
    gridCopy = gridToCopy;
}

void master::on_vccVal_valueChanged(double arg)
{

    if(arg <= 0)
    {
        QMessageBox::information(
                    this,
                    tr("warning!"),
                    tr("Vcc = 0 means no input tensions. Simulation would not make sense \n Setting to default 1") );
        ui->vccVal->setValue(1);
    }
}

CRACKS master::drawCrackPoint()
{
    bool stop = true;
    int i0_index;
    int j0_index;

    int dim = mainGrid.size()-1;

    while(stop == true)
    {
        i0_index = rand()% (dim-1)+1;
        j0_index = rand()% (dim);

        if(cracksGrid[i0_index][j0_index] == 0)
        {
            cracksGrid[i0_index][j0_index] = 1;
            stop = false;
        }
    }

    cracksGrid[i0_index][j0_index] = 1;

    return cracksGrid;
}

bool master::checkCrackPoint(int i, int j)
{
    if(cracksGrid.at(i).at(j) == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool master::checkRightToCrackPoint(int i, int j)
{
    if(j > 0)
    {
        if(cracksGrid[i][j-1] == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else return false;
}

bool master::checkBelowCrackPoint(int i, int j)
{
    int dim = cracksGrid.size();

    if(i > 0 && i < dim-1)
    {
        if(cracksGrid[i-1][j] == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else return false;
}

bool master::checkBelowRightToCrackPoint(int i, int j)
{
    int dim = cracksGrid.size();

    if(i > 0 && i < dim-1 && j > 0)
    {
        if(cracksGrid[i-1][j-1] == 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else return false;
}

void master::clearing(bool (&bools)[8], unsigned int &mode)
{
    for(unsigned int i=0; i<8; i++)
    {
        bools[0] = false;
    }

    mode = 0;
}

bool master::ifTop(int i)
{
    if(i > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool master::ifLeft(int j)
{
    if(j > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool master::ifRight(GRID &matrix, int j)
{
    if(j < int(matrix.size()-2))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool master::ifDensityRight(GRID &matrix, int j)
{
    if(j < int(matrix.size()-1))
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool master::ifDown(GRID &vector, int i)
{
    if(i < int(vector.size()-1))
    {
        return true;
    }
    else
    {
        return false;
    }
}

int master::operationMode(bool &top, bool &left, bool &right, bool &down)
{
    int mode = 0;

    mode = nodeSurroundings(top, left, right, down);

    return mode;
}

int master::densityOperationMode(bool &dTop, bool &dLeft, bool &dRight, bool &dDown)
{
    int dMode = 0;

    dMode = densityNodeSurroundings(dTop, dLeft, dRight, dDown);

    return dMode;
}

void master::nodeCalculus(int i, int j, int mode)
{
    switch(mode)
    {
    case 0:
        mainGrid[i][j] = 0;
        break;
    case 1:
        mainGrid[i][j] = vcc;
        break;
    case 2:
        mainGrid[i][j] = (mainGrid[i-1][j] + timesTwo*mainGridCopy[i][j+1] + mainGridCopy[i+1][j]) /divFour;
        break;
    case 3:
        mainGrid[i][j] = (mainGrid[i-1][j] + timesTwo*mainGrid[i][j-1] + mainGridCopy[i+1][j]) /divFour;
        break;
    case 4:
        mainGrid[i][j] = (mainGrid[i-1][j] + mainGridCopy[i][j+1] + mainGridCopy[i+1][j] + mainGrid[i][j-1]) /divFour;
        break;
    default:
        break;
    }
}

void master::densityNodeCalculus(int i, int j, int di, int dj, int dMode)
{
    switch(dMode)
    {
    case 11:
        calculateLeftTop(i, j, di, dj);
        break;
    case 12:
        calculateUpperRow(di, dj);
        break;
    case 13:
        calculateRightTop(i, j, di, dj);
        break;
    case 14:
        calculateLeftCol(di, dj);
        break;
    case 15:
        calculateCenter(di, dj);
        break;
    case 16:
        calculateRightCol(di, dj);
        break;
    case 17:
        calculateLeftDown(i, j, di, dj);
        break;
    case 18:
        calculateDownRow(di, dj);
        break;
    case 19:
        calculateRightDown(i, j, di, dj);
        break;
    default:
        break;
    }
}

void master::densitySweep(int i, int j, int di, int dj)
{
    bool dTop;
    bool dLeft;
    bool dRight;
    bool dDown;
    int dMode;

    dTop = ifTop(di);
    dLeft = ifLeft(dj);
    dRight = ifDensityRight(densityGrid, dj);
    dDown = ifDown(densityGrid, di);

    //-----------------------------------------------------------------------------------//
    dMode = densityOperationMode(dTop, dLeft, dRight, dDown);

    densityNodeCalculus(i, j, di, dj, dMode);
}

int master::nodeSurroundings(bool &top, bool &left, bool &right, bool &down)
{
    int mode = 0;

    if(top && left && right && down)
    {
        mode = 4;
    }
    else if(top && left && down)
    {
        mode = 3;
    }
    else if(top && right && down)
    {
        mode = 2;
    }
    else if(top && left && right)
    {
        mode = 0;
    }
    else if(left && right && down)
    {
        mode = 1;
    }
    else if((top && right) || (top && left))
    {
        mode = 0;
    }
    else
    {
        mode = 1;
    }

    return mode;
}

/*
 * similarly to nodeSurroundings but for density grid
 */
int master::densityNodeSurroundings(bool &top, bool &left, bool &right, bool &down)
{
    int dMode = 0;

    if(top && left && right && down)
    {
        dMode = 15;
    }
    else if(left && right && down)
    {
        dMode = 12;
    }
    else if(top && left && down)
    {
        dMode = 16;
    }
    else if(top && right && down)
    {
        dMode = 14;
    }
    else if(top && left && right)
    {
        dMode = 18;
    }
    else if(right && down)
    {
        dMode = 11;
    }
    else if(left && down)
    {
        dMode = 13;
    }
    else if(top && right)
    {
        dMode = 17;
    }
    else if(top && left)
    {
        dMode = 19;
    }
    else dMode = 20;

    return dMode;
}

/*************************************/
void master::calculateLeftTop(int i, int j, int di, int dj)
{
    if(j-1 < 0)
    {
        generalFunction(1, density);
        complex<double> k1 = licznik/mianownik2;

        //densityGrid[di][dj] = k1*(p*mainGrid[i-1][j] + p*densityGridCopy[di][dj+1] + m*densityGridCopy[di+1][dj]);
        densityGrid[di][dj] = k1*(m*mainGrid[i-1][j] + m*densityGridCopy[di][dj+1] + p*densityGridCopy[di+1][dj]);
    }
    else
    {
        generalFunction(1, density);
        complex<double> k1 = licznik/mianownik;

        //densityGrid[di][dj] = k1*(p*mainGrid[i-1][j] + m*mainGrid[i][j-1] + p*densityGridCopy[di][dj+1] + m*densityGridCopy[di+1][dj]);
        densityGrid[di][dj] = k1*(m*mainGrid[i-1][j] + p*mainGrid[i][j-1] + m*densityGridCopy[di][dj+1] + p*densityGridCopy[di+1][dj]);
    }
}

void master::calculateUpperRow(int di, int dj)
{
    generalFunction(density, density);
    complex<double> k1 = licznik/mianownik2;

    //	cout << "UpperRow" << endl;
    densityGrid[di][dj] = k1*m*(densityGrid[di][dj-1] + densityGridCopy[di][dj+1] + densityGridCopy[di+1][dj]);
}

void master::calculateRightTop(int i, int j, int di, int dj)
{
    //	cout << "RightTop" << endl;
    if(j+1 > int(mainGrid.size()-2))
    {
        generalFunction(1, density);
        complex<double> k1 = licznik/mianownik2;

//        densityGrid[di][dj] = k1*(p*mainGrid.at(i-1).at(j) + m*densityGrid.at(di).at(dj-1) + m*densityGridCopy.at(di+1).at(dj));
        densityGrid[di][dj] = k1*(m*mainGrid.at(i-1).at(j) + p*densityGrid.at(di).at(dj-1) + p*densityGridCopy.at(di+1).at(dj));
    }
    else
    {
        generalFunction(1, density);
        complex<double> k1 = licznik/mianownik;

//        densityGrid[di][dj] = k1*(p*mainGrid.at(i-1).at(j) + p*mainGridCopy.at(i).at(j+1) + m*densityGrid.at(di).at(dj-1) + m*densityGridCopy.at(di+1).at(dj));
        densityGrid[di][dj] = k1*(m*mainGrid.at(i-1).at(j) + m*mainGridCopy.at(i).at(j+1) + p*densityGrid.at(di).at(dj-1) + p*densityGridCopy.at(di+1).at(dj));
    }
}

void master::calculateLeftCol(int di, int dj)
{
    generalFunction(density, density);
    complex<double> k1 = licznik/mianownik2;

    //	cout << "LeftCol" << endl;
    //    densityGrid[di][dj] = densityGrid[di-1][dj] + densityGridCopy[di][dj+1] + densityGridCopy[di+1][dj];
    densityGrid[di][dj] = k1*m*(densityGrid[di-1][dj] + densityGridCopy[di][dj+1] + densityGridCopy[di+1][dj]);
}

void master::calculateCenter(int di, int dj)
{
    generalFunction(density, density);
    complex<double> k1 = licznik/mianownik;

    //	cout << "Center" << endl;
    densityGrid[di][dj] = k1*m*(densityGrid[di-1][dj] + densityGrid[di][dj-1] + densityGridCopy[di+1][dj] + densityGridCopy[di][dj+1]);
}

void master::calculateRightCol(int di, int dj)
{
    generalFunction(density, density);
    complex<double> k1 = licznik/mianownik2;

    //	cout << "RightCol" << endl;
    densityGrid[di][dj] = k1*m*(densityGrid[di-1][dj] + densityGrid[di][dj-1] + densityGridCopy[di+1][dj]);
}

void master::calculateLeftDown(int i, int j, int di, int dj)
{
    //	cout << "LeftDown" << endl;
    if(j-1 < 0)
    {
        generalFunction(1, density);
        complex<double> k1 = licznik/mianownik2;

//        densityGrid[di][dj] = k1*(m*mainGridCopy.at(i+1).at(j) + p*densityGrid.at(di-1).at(dj) + p*densityGridCopy.at(di).at(dj+1));
        densityGrid[di][dj] = k1*(p*mainGridCopy.at(i+1).at(j) + m*densityGrid.at(di-1).at(dj) + m*densityGridCopy.at(di).at(dj+1));
    }
    else
    {
        generalFunction(1, density);
        complex<double> k1 = licznik/mianownik;

//        densityGrid[di][dj] = k1*(m*mainGrid.at(i).at(j-1) + m*mainGridCopy.at(i+1).at(j) + p*densityGrid.at(di-1).at(dj) + p*densityGridCopy.at(di).at(dj+1));
        densityGrid[di][dj] = k1*(p*mainGrid.at(i).at(j-1) + p*mainGridCopy.at(i+1).at(j) + m*densityGrid.at(di-1).at(dj) + m*densityGridCopy.at(di).at(dj+1));
    }
}

void master::calculateDownRow(int di, int dj)
{
    generalFunction(density, density);
    complex<double> k1 = licznik/mianownik2;

    //	cout << "DownRow" << endl;
    densityGrid[di][dj] = k1*m*(densityGrid[di-1][dj] + densityGrid[di][dj-1] + densityGridCopy[di][dj+1]);
}

void master::calculateRightDown(int i, int j, int di, int dj)
{
    //	cout << "RightDown" << endl;
    if( j+1 >  int(mainGrid.size()-2))
    {
        generalFunction(1, density);
        complex<double> k1 = licznik/mianownik2;

//        densityGrid[di][dj] = k1*(p*densityGrid.at(di-1).at(dj) + m*densityGrid.at(di).at(dj-1) + m*mainGridCopy.at(i+1).at(j));
        densityGrid[di][dj] = k1*(m*densityGrid.at(di-1).at(dj) + p*densityGrid.at(di).at(dj-1) + p*mainGridCopy.at(i+1).at(j));
    }
    else
    {
        generalFunction(1, density);
        complex<double> k1 = licznik/mianownik;

//        densityGrid[di][dj] = k1*(p*densityGrid.at(di-1).at(dj) + m*densityGrid.at(di).at(dj-1) + m*mainGridCopy.at(i+1).at(j) + p*mainGridCopy.at(i).at(j+1));
        densityGrid[di][dj] = k1*(m*densityGrid.at(di-1).at(dj) + p*densityGrid.at(di).at(dj-1) + p*mainGridCopy.at(i+1).at(j) + m*mainGridCopy.at(i).at(j+1));
    }
}
/*****************************************************************************************/

void master::determineEnds(int i, int j, unsigned int &forDiEnd, unsigned int &forDjEnd)
{
    int rBorder = mainGrid.size()-2;
    int dBorder = mainGrid.size()-2;
    int dIndexJ = mainGrid.size();

    if(j == rBorder)
    {
        forDjEnd = 1;
    }
    else forDjEnd = dIndexJ-1;

    if(i == dBorder)
    {
        forDiEnd = dBorder+1;
    }
    else forDiEnd = 1;

    if(j == rBorder && i == dBorder-1) forDiEnd = dBorder+1;
}

void master::generalFunction(int mVal, int pVal)
{
    complex<double> times;

    times.real(4);

    m.real(mVal);
    p.real(pVal);

    licznik.real(timesTwo.real() * m.real() * p.real() / (m.real() + p.real()));
    licznik.imag(0);

    //for 4nodes
    mianownik.real(times.real() * m.real() * p.real());
    mianownik.imag(pulsation * tau);

    //for 3 nodes - borders
    mianownik2.real((times.real()-1) * m.real() * p.real());
    mianownik2.imag(mianownik.imag());
}

void master::calculateCurrents()
{
    int dim = dimension;

    complex<double> tcurr;
    complex<double> rVal;
    curr1.clear();

    rVal.real(master::rVal);
    rVal.imag(0);

    complex<double> uno(1,0);

    for(int j = 0; j < dim; j++)
    {
        if(j == 0 || j == dim-1)
        {
            curr1.push_back((uno-mainGrid[1][j])/(timesTwo*rVal));
        }
        else curr1.push_back((uno-mainGrid[1][j])/rVal);
    }

    for(auto i : curr1)
    {
        tcurr += i;
    }
    totalCurr.push_back(tcurr);
}

int master::mainCalculationAlgorithm()
{
//    QList<float> elka;
//    for (float i = 0.0; i < 15000; i=i+0.001)
//    {
//        elka.push_back(i);
//    }

    unsigned int indexI = mainGrid.size();
    unsigned int indexJ = mainGrid.size()-1;

    unsigned int dIndexI = densityGrid.size();
    unsigned int dIndexJ = densityGrid.size();

    bool top = false;
    bool left = false;
    bool right = false;
    bool down = false;

    bool isCrackPoint = false;
    bool isRightToCrackPoint = false;
    bool isBelowCrackPoint = false;
    bool isBelowRightToCrackPoint = false;

    bool bools[8];
    bools[0] = top;
    bools[1] = left;
    bools[2] = right;
    bools[3] = down;
    bools[4] = isCrackPoint;
    bools[5] = isRightToCrackPoint;
    bools[6] = isBelowCrackPoint;
    bools[7] = isBelowRightToCrackPoint;

    unsigned int mode = 0;

    int crackStop = (indexI*indexJ)-(indexJ*2);

    for(int crackNo = 0; crackNo < crackStop+1; crackNo++)
    {
        if(crackNo > 0) drawCrackPoint();

        int licz1 = iterationCount;
        licz1 = 1;

        for(double f=0; f<frequency; f=f+0.1)
        {
            fillWithZeros(mainGrid);
            fillWithZeros(mainGridCopy);
            fillWithZeros(densityGrid);
            fillWithZeros(densityGridCopy);

            pulsation = 2*pi*f;
            divFour.imag(pulsation*tau);

            for(int iteracje = 0; iteracje < iterationCount; iteracje++)
            {
                copyGrids(mainGrid, mainGridCopy);
                copyGrids(densityGrid, densityGridCopy);

                for(unsigned int i=0; i<indexI; i++)
                {
                    for(unsigned int j=0; j<indexJ; j++)
                    {
                        clearing(bools, mode);

                        isCrackPoint = checkCrackPoint(i, j);
                        isRightToCrackPoint = checkRightToCrackPoint(i, j);
                        isBelowCrackPoint = checkBelowCrackPoint(i, j);
                        isBelowRightToCrackPoint = checkBelowRightToCrackPoint(i, j);

                        top = ifTop(i);
                        left = ifLeft(j);
                        right = ifRight(mainGrid, j);
                        down = ifDown(mainGrid, i);

                        //-----------------------------------------------------------------------------------//
                        mode = operationMode(top, left, right, down);

                        unsigned int forDiEnd = 0;
                        unsigned int forDjEnd = 0;

                        determineEnds(i, j, forDiEnd, forDjEnd);

                        if(isCrackPoint)
                        {
                            for(unsigned int di = 0; di < forDiEnd; di++)
                            {
                                for(unsigned int dj = 0; dj < forDjEnd; dj++)
                                {
                                    densitySweep(i, j, di, dj);
                                }
                            }
                            mainGrid[i][j] = densityGrid[0][0];
                        }
                        else if(isRightToCrackPoint)
                        {
                            unsigned int di = 0;
                            unsigned int dj = dIndexJ-1;

                            densitySweep(i, j, di, dj);

                            for(di = 1; di < dIndexI-1; di++)
                            {
                                for(dj = 0; dj < dIndexJ; dj++)
                                {
                                    densitySweep(i, j, di, dj);
                                }
                            }

                            mainGrid[i][j] = densityGrid[0][dIndexJ-1];
                        }
                        else if(isBelowCrackPoint)
                        {
                            for(unsigned int di = dIndexI-1; di < dIndexI; di++)
                            {
                                for(unsigned int dj = 0; dj < forDjEnd; dj++)
                                {
                                    densitySweep(i, j, di, dj);
                                }
                            }
                            mainGrid[i][j] = densityGrid[dIndexI-1][0];
                        }
                        else if(isBelowRightToCrackPoint)
                        {
                            unsigned int di = dIndexI-1;
                            unsigned int dj = dIndexJ-1;
                            densitySweep(i, j, di, dj);

                            mainGrid[i][j] = densityGrid[dIndexI-1][dIndexJ-1];
                        }
                        else nodeCalculus(i, j, mode);
                    }
                }
            }

            calculateCurrents();
            freqVect.push_back(f);
            pulsationVect.push_back(pulsation);
            wVect.push_back(mainGrid[1][1]);

            /*
             * wartość prądy wejściowego jest obliczana w każdej wewnętrznej iteracji
             * a potrzebna jest ostatnia wartość z iterowania, żeby mieć mniej więcej pewność, że
             * jest z obszaru nasycenia
             *
             * dlatego trzeba brać wartość wektora prądu zaczynając od wartości iteracji i
             * każdą kolejną powiększoną o liczbę iteracji
             *
             * a to po to aby móc sensownie wykreślić przebiegi It oraz O(i,j) od
             * częstotliwości lub pulsacji
             */
            iVect.push_back(totalCurr[licz1]);
            //licz1+=iterCounts;
            licz1++;
        }
//        freqVect.push_back(0);
//        pulsationVect.push_back(0);
//        wVect.push_back(0);
//        iVect.push_back(0);
    }
//    return elka.length();
    return 0;
}

void master::saveDataToFile(int saveMode)
{
    /*
     * zrobić poprawne zapisywanie danych do pliku tekstowego
     */
    QString currentPath = qApp->applicationName();

    QString fileName = QFileDialog::getSaveFileName(
                this,
                tr("Save File"),
                "../"+currentPath,
                "(*.kd);;(*.txt)"
                );

    QFile file(fileName);

    QTextStream out(&file);

    file.open(QIODevice::WriteOnly | QIODevice::Text);

    switch(saveMode)
    {
    case 1:
        out << "Input current values" << endl;
        forCurrentWriteFile(iVect, out);
        break;
    case 2:
        out << "Potential value in node (1, 1)" << endl;
        forCurrentWriteFile(wVect, out);
    case 3:
        out << "Potential value in node (1, 1)" << endl;
        forCurrentWriteFile(wVect, out);
        out << "Frequency" << endl;
        forCurrentWriteFile(freqVect, out);
        out << "Input current values" << endl;
        forCurrentWriteFile(iVect, out);
        break;
    }

    file.close();
}

void master::setSaveModeOne()
{
    saveDataToFile(1);
}

void master::setSaveModeTwo()
{
    saveDataToFile(2);
}

void master::setSaveModeThree()
{
    saveDataToFile(3);
}

template<typename Tt>
void master::forCurrentWriteFile(QList<Tt> & forSave, QTextStream & out)
{

    QLocale locale = QLocale("pl_PL");
    QString realPart;
    QString imagPart;

    typename QList<Tt>::iterator row;
    for(row=forSave.begin(); row!=forSave.end(); row++)
    {
        realPart = locale.toString(real(*row));
        imagPart = locale.toString(imag(*row));
//        out << real(*row) << "\t" << imag(*row) << endl;
        out << realPart << "\t" << imagPart << endl;
    }
}
