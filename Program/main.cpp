#include "master.h"
#include <QApplication>
#include <QMenuBar>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    master w;
    w.show();

    return a.exec();
}
